/*
 * The MIT License
 *
 * Copyright 2017 Oliver.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jgui.gui.argument;

import jgui.gui.ArgumentType;
import jgui.gui.argument.Argument;

/**
 *
 * @author Oliver
 */
public class CheckboxArgument extends Argument<Boolean>{

	/**
	 * Create a new Checkbox without a description.
	 *
	 * @param argumentName The name of the argument
	 * @param title        The title to show on the GUI
	 * @param defValue     The default value of the checkbox
	 */
	public CheckboxArgument(String argumentName, String title, boolean defValue){
		this(argumentName, title, "", defValue);
	}

	/**
	 * Create a new Checkbox with a description.
	 *
	 * @param argumentName The name of the argument
	 * @param title        The title to show on the GUI
	 * @param defValue     The default value of the checkbox
	 * @param desc			A short description of what the argument is
	 */
	public CheckboxArgument(String argumentName, String title, String desc, boolean defValue){
		super(argumentName, title, desc);
		this.setValue(defValue);
	}

	@Override
	public ArgumentType getType(){
		return ArgumentType.CHECKBOX;
	}
	
	public boolean getBool(){
		return (boolean)super.getValue();
	}

	@Override
	public boolean isEmpty(){
		return this.value == null;
	}
}
