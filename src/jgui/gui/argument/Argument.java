package jgui.gui.argument;

import jgui.gui.ArgumentType;

/**
 * This is more of a way to store data than an actual class. It has no
 * behaviour, it just stores related data about an argument in an easy way to
 * pass around and work with.
 * <p>
 * @author Oliver
 * @param <T> The type of value the argument holds
 */
public abstract class Argument<T>{
	protected T value;
	private final String id;
	private final String title;
	private final String desc;
	private boolean emptyAllowed = true;

	/**
	 * Store all the information needed about the argument to refer to it later
	 * <p>
	 * @param id The name to refer to the argument by (not shown on
	 *                     the GUI)
	 * @param title        A title to put next to the argument input on the GUI
	 * @param desc         A small description of what the argument is, it gets
	 *                     shown much smaller next to the argument input on the
	 *                     GUI
	 */
	public Argument(String id, String title, String desc){
		this.id = id;
		this.title = title;
		this.desc = desc;
	}

	/**
	 * Does the argument have a description?
	 * <p>
	 * @return true if the description isn't empty
	 */
	public boolean hasDescription(){
		return !desc.isEmpty();
	}

	/**
	 * Gets the type of the argument
	 * <p>
	 * @return Type of the argument
	 */
	public abstract ArgumentType getType();
	
	
	public void setEmptyAllowed(boolean emptyAllowed){
		this.emptyAllowed = emptyAllowed;
	}
	/**
	 * Returns true if the argument can be passed without any additional parameters
	 * eg. if "-optional 32" and "-optional" are both valid
	 * Returns false if the argument needs an extra parameter
	 * eg. "-time 12" is valid but "-time" isn't
	 * @return true by default
	 */
	public boolean emptyAllowed(){
		return this.emptyAllowed;
	}

	/**
	 * Gets the name of the argument
	 * <p>
	 * @return ID of the argument
	 */
	public String getID(){
		return id;
	}

	/**
	 * Gets the title of the argument
	 * <p>
	 * @return Title of the argument
	 */
	public String getTitle(){
		return title;
	}

	/**
	 * Gets the description of the argument
	 * <p>
	 * @return Description of the argument
	 */
	public String getDescription(){
		return desc;
	}
	
	public T getValue(){
		return this.value;
	}
	
	public void setValue(T newValue){
		this.value = newValue;
	}
	
	public abstract boolean isEmpty();
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder(50);
		
		sb
			.append(this.getType())
			.append(": {")
			.append("\n  ID: ")
			.append(this.getID())
			.append("\n  Title: ")
			.append(this.getTitle())
			.append("\n  Value: ") 
			.append(this.getValue())
			.append("\n  Description: ") 
			.append(this.getDescription())
			.append("\n}")
		;
		return sb.toString();
	}
}
