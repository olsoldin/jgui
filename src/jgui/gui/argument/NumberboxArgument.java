/*
 * The MIT License
 *
 * Copyright 2017 Oliver.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jgui.gui.argument;

import jgui.gui.ArgumentType;
import jgui.gui.argument.Argument;

/**
 *
 * @author Oliver
 */
public class NumberboxArgument extends Argument<Integer>{

	public NumberboxArgument(String argumentName, String title, int defValue){
		this(argumentName, title, "", defValue);
	}

	public NumberboxArgument(String argumentName, String title, String desc, int defValue){
		super(argumentName, title, desc);
		this.setValue(defValue);
		this.setEmptyAllowed(false);
	}

	@Override
	public ArgumentType getType(){
		return ArgumentType.NUMBERBOX;
	}

	/**
	 * An Integer can only be empty if it is null
	 * @return isNull
	 */
	@Override
	public boolean isEmpty(){
		return this.value == null;
	}
}
