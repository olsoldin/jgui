/*
 * The MIT License
 *
 * Copyright 2017 Oliver.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jgui.gui.argument;

import java.util.Collections;
import java.util.List;
import jgui.gui.ArgumentType;
import jgui.gui.argument.Argument;

/**
 * This argument has a drop down list of all the possible options.
 *
 * @author Oliver
 */
public class DropdownArgument extends Argument<String>{
	private List<String> options;

	/**
	 * Create a new Dropdown without a description.
	 *
	 * @param argumentName The name of the argument
	 * @param title        The title to show on the GUI
	 * @param defValue     The index of the default option in the options list
	 * @param options      The list of options to give the user
	 */
	public DropdownArgument(String argumentName, String title, int defValue, List<String> options){
		this(argumentName, title, "", defValue, options);
	}

	/**
	 * Create a new Dropdown with a description.
	 *
	 * @param argumentName The name of the argument
	 * @param title        The title to show on the GUI
	 * @param defValue     The index of the default option in the options list
	 * @param options      The list of options to give the user
	 * @param desc			A short description of what the argument is
	 */
	public DropdownArgument(String argumentName, String title, String desc, int defValue, List<String> options){
		super(argumentName, title, desc);
		this.options = options;
		this.setValue(defValue);
	}

	@Override
	public ArgumentType getType(){
		return ArgumentType.DROPDOWN;
	}
	
	public final void setValue(int value){
		super.setValue(this.options.get(value));
	}
	
	public List<String> getOptions(){
		return Collections.unmodifiableList(this.options);
	}

	@Override
	public boolean isEmpty(){
		return this.value == null || this.value.isEmpty();
	}
}
