/*
 * The MIT License
 *
 * Copyright 2018 Oliver.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jgui.gui.argument;

import jgui.gui.ArgumentType;
import jgui.gui.Range;

/**
 *
 * @author Oliver
 */
public class SliderArgument extends Argument<Integer>{

	/*
	 * Needs:
	 * id
	 * title
	 * description
	 * min
	 * default
	 * max
	 */
	private Range range;

	public SliderArgument(String argumentName, String title, Range range){
		this(argumentName, title, "", range);
	}

	public SliderArgument(String argumentName, String title, String desc, Range range){
		super(argumentName, title, desc);
		this.range = range;
		this.setValue(range);
	}

	@Override
	public void setValue(Integer value){
		this.range.current = value;
		super.setValue(value);
	}

	public void setValue(Range range){
		this.setValue(range.current);
	}

	public int getMin(){
		return this.range.MIN;
	}

	public int getMax(){
		return this.range.MAX;
	}

	@Override
	public void setEmptyAllowed(boolean emptyAllowed){
		// dont change anything
	}

	@Override
	public ArgumentType getType(){
		return ArgumentType.SLIDER;
	}

	@Override
	public boolean isEmpty(){
		// TODO: change this
		return true;
	}
}
