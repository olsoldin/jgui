package jgui.gui;

import jgui.gui.argument.Argument;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Group;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;
import jgui.gui.argument.CheckboxArgument;

/**
 *
 * @author Oliver
 */
public class GUI extends JFrame{

	private static final String PREFIX = "JGUI - ";
	private static final long serialVersionUID = 1L;

	private final Map<String, Component> components = new HashMap(1);
	private final List<Argument> arguments = new ArrayList<>(1);
	private JPanel panel = new JPanel();
	private final Border panelBorder = new LineBorder(new Color(150, 150, 150));

	private final GroupLayout layout;

	private Group hGroup;
	private Group vGroup;

	private static int maxWidth = 0;

	private GeneralPath gp;
	private Component pathTxt;
	private String programPath = "";

	private final MouseInputListener resizeHook = new MouseInputAdapter(){
		private Point startPos = null;

		@Override
		public void mousePressed(MouseEvent e){
			if(gp.contains(e.getPoint())){
				startPos = new Point(getWidth() - e.getX(), getHeight() - e.getY());
			}
		}

		@Override
		public void mouseReleased(MouseEvent e){
			startPos = null;
		}

		@Override
		public void mouseMoved(MouseEvent e){
			if(gp.contains(e.getPoint())){
				setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
			}else{
				setCursor(Cursor.getDefaultCursor());
			}
		}

		@Override
		public void mouseDragged(MouseEvent e){
			if(startPos != null){
				int dx = e.getX() + startPos.x;
				int dy = e.getY() + startPos.y;

				setSize(dx, dy);
				repaint();
			}
		}
	};

	public GUI(){
		this("");
	}

	public GUI(String title){
		super(PREFIX + title);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		layout = new GroupLayout(this.getContentPane());
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);

		this.getContentPane().setLayout(new FlowLayout());

		// Add custom resize handling to stop most of the flickering when resizing the window.
		this.addMouseMotionListener(resizeHook);
		this.addMouseListener(resizeHook);
		this.setResizable(false);

		// Add the menu bar
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
		menu.setMnemonic(KeyEvent.VK_F);
		menu.getAccessibleContext().setAccessibleDescription("Menu items relating to file");

		JMenuItem menuRun = new JMenuItem("Run");
		menuRun.setMnemonic(KeyEvent.VK_R);
		menuRun.getAccessibleContext().setAccessibleDescription("Run the selected program");
		menuRun.addActionListener((e) -> {
			run(((JTextField) pathTxt).getText());
		});

		JMenuItem menuOpen = new JMenuItem("Open");
		menuOpen.setMnemonic(KeyEvent.VK_O);
		menuOpen.getAccessibleContext().setAccessibleDescription("Open a new file to run");
		menuOpen.addActionListener((e) -> {
			// TODO: open a jfilechooser and load it into pathTxt
			System.out.println("Opening new file");
		});

		menu.add(menuRun);
		menu.add(menuOpen);
		menuBar.add(menu);
		this.setJMenuBar(menuBar);
	}

	public void begin(){
		begin("");
	}

	public void begin(String path){
		this.setSize(maxWidth + 20, 640);

		pathTxt = new JTextField(path);
		pathTxt.setPreferredSize(new Dimension(200, 25));

		JButton runBtn = new JButton("Run");
		runBtn.addActionListener((ActionEvent e) -> {
			run(((JTextField) pathTxt).getText());
		});

		JButton showOptionsBtn = new JButton("Show options");
		showOptionsBtn.addActionListener((ActionEvent e) -> {
			showOptions();
		});

		this.add(pathTxt);
		this.add(runBtn);
		this.add(showOptionsBtn);

		this.setVisible(true);
	}

	public void run(String path){
		StringBuilder sb = new StringBuilder(path);

		arguments.forEach((arg) -> {
			// If the argument doesn't allow empty values, and it's empty, then skip it
			if(!arg.emptyAllowed()){
				if(arg.isEmpty()){
					return;
				}
			}
			String val = arg.getValue().toString();
			// Make sure arguments with spaces in have surrounding quotes
			if(val.contains(" ")){
				val = '"' + val + '"';
			}

			
			if(arg.getType() == ArgumentType.CHECKBOX){
				if(((CheckboxArgument)arg).getBool()){
					sb.append(" -");
					sb.append(arg.getID());
				}
			}else{
				// eg. -title value
				sb.append(" -");
				sb.append(arg.getID());
				sb.append(" ");
				sb.append(val);				
			}


		});

		// TODO: run this as a command
		System.out.println(sb);
	}

	public void showOptions(){
		StringBuilder sb = new StringBuilder();

		arguments.forEach((arg) -> {
			sb.append(arg.toString());
			sb.append("\n");
		});

		System.out.println(sb);
	}

	/**
	 * For some reason, resizing with the default dragging is extremely laggy and horrible
	 * so in order to make it smoother, we need to override the paint method and draw a small
	 * area in the bottom right to resize the window based on the mouse position.
	 * Somehow this makes it smooth...
	 *
	 * @param g
	 */
	@Override
	public void paint(Graphics g){
		super.paint(g);
		int w = getWidth();
		int h = getHeight();
		g.setColor(new Color(150, 150, 150, 200));
		g.drawLine(w - 7, h, w, h - 7);
		g.drawLine(w - 11, h, w, h - 11);
		g.drawLine(w - 15, h, w, h - 15);

		gp = new GeneralPath();
		gp.moveTo(w - 17, h);
		gp.lineTo(w, h - 17);
		gp.lineTo(w, h);
		gp.closePath();
	}

	public void startGroup(){
		startGroup("");
	}

	public void startGroup(String title){
		panel = new JPanel();
		if(title == null || title.isEmpty()){
			panel.setBorder(panelBorder);
		}else{
			panel.setBorder(new TitledBorder(panelBorder, title));
		}

		hGroup = layout.createSequentialGroup();
		vGroup = layout.createSequentialGroup();
	}

	public void endGroup(){
		hGroup.addComponent(panel);
		vGroup.addComponent(panel);

		layout.setHorizontalGroup(hGroup);
		layout.setVerticalGroup(vGroup);
		this.pack();
		maxWidth = Math.max(panel.getWidth(), maxWidth);

		panel = null;
	}

	/**
	 * Adds the requested type of argument to the GUI
	 * <p>
	 * @param arg The argument to add to the GUI
	 */
	public void addArgument(Argument arg){
		Component comp = ComponentBuilder.build(panel, arg);
		components.put(arg.getID(), comp);
		arguments.add(arg);
	}
}
