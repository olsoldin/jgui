package jgui.gui;

/**
 *
 * @author Oliver
 */
public enum ArgumentType {
	TEXTBOX, CHECKBOX, NUMBERBOX, DROPDOWN, PATH, DATE, SLIDER;
	
	
	@Override
	public String toString(){
		return this.name();
	}
}
