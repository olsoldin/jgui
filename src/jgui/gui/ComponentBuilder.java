package jgui.gui;

import java.awt.Checkbox;
import java.awt.Component;
import java.awt.Font;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import jgui.gui.argument.CheckboxArgument;
import jgui.gui.argument.DropdownArgument;
import jgui.gui.argument.NumberboxArgument;
import jgui.gui.argument.Argument;
import jgui.gui.argument.PathArgument;
import jgui.gui.argument.SliderArgument;
import jgui.gui.argument.TextboxArgument;

/**
 * @author Oliver
 */
public class ComponentBuilder{

	private static final Border BORDER_EMPTY = new EmptyBorder(10, 10, 10, 10);
	private static final Font FONT_DESCRIPTION = new Font("Ariel", Font.PLAIN, 10);
	private static final long serialVersionUID = 1L;

	/**
	 * ComponentBuilder doesn't have any state, all of the functions take
	 * arguments and return values
	 */
	private ComponentBuilder(){
	}

	public static Component build(JPanel panel, Argument arg){
		Component comp = new Component(){
			private static final long serialVersionUID = 1L;
		};
		switch(arg.getType()){
			case CHECKBOX:
				comp = ComponentBuilder.buildCheckbox(panel, (CheckboxArgument) arg);
			case DATE:
				// TODO: Add date picker
				break;
			case PATH:
				comp = ComponentBuilder.buildPath(panel, (PathArgument) arg);
				break;
			case DROPDOWN:
				comp = ComponentBuilder.buildDropdown(panel, (DropdownArgument) arg);
				break;
			case NUMBERBOX:
				comp = ComponentBuilder.buildNumberbox(panel, (NumberboxArgument) arg);
				break;
			case TEXTBOX:
				comp = ComponentBuilder.buildTextBox(panel, (TextboxArgument) arg);
				break;
			case SLIDER:
				comp = ComponentBuilder.buildSlider(panel, (SliderArgument) arg);
				break;
		}
		return comp;
	}

	/**
	 * Creates a checkbox, adds it to the given panel and returns the created
	 * checkbox to add to an array
	 * <p>
	 * @param panel The main panel to add the checkbox to
	 * @param arg   The argument to make a checkbox for.
	 * <p>
	 * @return The checkbox as a component.
	 */
	public static Component buildCheckbox(JPanel panel, CheckboxArgument arg){
		Component comp = new Checkbox(arg.getTitle(), arg.getBool());

		// Update the argument
		((Checkbox) comp).addItemListener((ItemEvent e) -> {
			boolean val = ((Checkbox) comp).getState();
			arg.setValue(val);
		});

		JPanel pnl = new JPanel();
		pnl.setBorder(BORDER_EMPTY);

		pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));
		pnl.add(comp);

		if(arg.hasDescription()){
			addDesc(pnl, arg.getDescription());
		}

		panel.add(pnl);

		return comp;
	}

	/**
	 * Creates a textbox, adds it to the given panel and returns the created
	 * textbox to add to an array
	 *
	 * @param panel The main panel to add the textbox to
	 * @param arg   The argument to make a textbox for
	 *
	 * @return The textbox as a component
	 */
	public static Component buildPath(JPanel panel, PathArgument arg){
		Component comp = new TextField(arg.getValue());

		// Update the argument
		((TextField) comp).addKeyListener(new KeyListener(){
			@Override
			public void keyTyped(KeyEvent e){
			}

			@Override
			public void keyPressed(KeyEvent e){
			}

			@Override
			public void keyReleased(KeyEvent e){
				String val = ((TextField) comp).getText();
				arg.setValue(val);
			}
		});

		JLabel lblTitle = new JLabel(arg.getTitle());
		JButton browseBtn = new JButton("Browse...");
		JPanel pnl = new JPanel();
		pnl.setBorder(BORDER_EMPTY);

		pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));
		pnl.add(lblTitle);
		pnl.add(comp);

		if(arg.hasDescription()){
			addDesc(pnl, arg.getDescription());
		}

		browseBtn.addActionListener((ActionEvent e) -> {
			File file;

			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

			fileChooser.showDialog(panel, "Select");

			file = fileChooser.getSelectedFile();

			// Only update the path if the file exists and is not the same as the saved one
			if(file != null && file.exists() && !file.getPath().equals(arg.getValue())){
				arg.setValue(file.getPath());
			}

			((TextField) comp).setText(arg.getValue());
		});

		pnl.add(browseBtn);
		panel.add(pnl);

		return comp;
	}

	/**
	 * Creates a textbox, adds it to the given panel and returns the created
	 * textbox to add to an array
	 *
	 * @param panel The main panel to add the textbox to
	 * @param arg   The argument to make a textbox for
	 *
	 * @return The textbox as a component
	 */
	public static Component buildTextBox(JPanel panel, TextboxArgument arg){
		Component comp = new TextField(arg.getValue());

		// Update the argument
		((TextField) comp).addKeyListener(new KeyListener(){
			@Override
			public void keyTyped(KeyEvent e){
			}

			@Override
			public void keyPressed(KeyEvent e){
			}

			@Override
			public void keyReleased(KeyEvent e){
				String val = ((TextField) comp).getText();
				arg.setValue(val);
			}
		});

		JLabel lblTitle = new JLabel(arg.getTitle());
		JPanel pnl = new JPanel();
		pnl.setBorder(BORDER_EMPTY);

		pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));
		pnl.add(lblTitle);
		pnl.add(comp);

		if(arg.hasDescription()){
			addDesc(pnl, arg.getDescription());
		}
		panel.add(pnl);

		return comp;
	}

	public static Component buildNumberbox(JPanel panel, NumberboxArgument arg){
		SpinnerNumberModel numberModel = new SpinnerNumberModel();
		numberModel.setValue(arg.getValue());
		Component comp = new JSpinner(numberModel);

		// Update the argument
		((JSpinner) comp).addChangeListener((ChangeEvent e) -> {
			Integer val = numberModel.getNumber().intValue();
			arg.setValue(val);
		});

		JLabel lblTitle = new JLabel(arg.getTitle());
		JPanel pnl = new JPanel();
		pnl.setBorder(BORDER_EMPTY);

		pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));
		pnl.add(lblTitle);
		pnl.add(comp);

		addDesc(pnl, arg.getDescription());

		panel.add(pnl);

		return comp;
	}

	public static Component buildDropdown(JPanel panel, DropdownArgument arg){
		Component comp = new JComboBox();

		arg.getOptions().forEach((opt) -> {
			((JComboBox) comp).addItem(opt);
		});

		((JComboBox) comp).setSelectedItem(arg.getValue());

		// Update the argument
		((JComboBox) comp).addActionListener((ActionEvent e) -> {
			int selectedIndex = ((JComboBox) comp).getSelectedIndex();
			arg.setValue(selectedIndex);
		});

		JLabel lblTitle = new JLabel(arg.getTitle());
		JPanel pnl = new JPanel();
		pnl.setBorder(BORDER_EMPTY);

		pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));
		pnl.add(lblTitle);
		pnl.add(comp);

		addDesc(pnl, arg.getDescription());

		panel.add(pnl);

		return comp;
	}

	public static Component buildSlider(JPanel panel, SliderArgument arg){
		Component comp = new JSlider(arg.getMin(), arg.getMax(), arg.getValue());
		JLabel lblMin = new JLabel(arg.getMin() + "");
		JLabel lblValue = new JLabel(arg.getValue() + "");
		JLabel lblMax = new JLabel(arg.getMax() + "");
		
		((JSlider) comp).addChangeListener((e) -> {
			int newValue = ((JSlider) comp).getValue();
			lblValue.setText(newValue + "");
			arg.setValue(newValue);
		});

		Font smallFont = new Font(UIManager.getFont("Label.font").getName(), Font.PLAIN, 10);
		lblMin.setFont(smallFont);
		lblMax.setFont(smallFont);
		
		JPanel pnlSlider = new JPanel();
		
		pnlSlider.setBorder(BORDER_EMPTY);
		pnlSlider.setLayout(new BoxLayout(pnlSlider, BoxLayout.X_AXIS));
		pnlSlider.add(lblMin);
		pnlSlider.add(comp);
		pnlSlider.add(lblMax);

		JLabel lblTitle = new JLabel(arg.getTitle());
		JPanel pnl = new JPanel();
		pnl.setBorder(BORDER_EMPTY);

		pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));
		pnl.add(lblTitle);
		pnl.add(pnlSlider);
		pnl.add(lblValue);

		addDesc(pnl, arg.getDescription());

		panel.add(pnl);

		return comp;
	}

	/**
	 * Adds a small description to the given panel, only if desc is not equal to
	 * ""
	 * <p>
	 * @param panel The JPanel to add the description to
	 * @param desc  The description to add to the panel
	 */
	public static void addDesc(JPanel panel, String desc){
		JLabel label = new JLabel(desc);
		label.setFont(FONT_DESCRIPTION);
		panel.add(label);
	}
}
