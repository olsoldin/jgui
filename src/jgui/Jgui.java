/*
 * The MIT License
 *
 * Copyright 2016 Oliver.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jgui;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.swing.SwingUtilities;
import jgui.gui.argument.CheckboxArgument;
import jgui.gui.argument.DropdownArgument;
import jgui.gui.argument.NumberboxArgument;
import jgui.annotation.Checkbox;
import jgui.annotation.Dropdown;
import jgui.annotation.Numberbox;
import jgui.gui.GUI;
import jgui.annotation.Path;
import jgui.annotation.Slider;
import jgui.annotation.Textbox;
import jgui.gui.Range;
import jgui.gui.argument.Argument;
import jgui.gui.argument.PathArgument;
import jgui.gui.argument.SliderArgument;
import jgui.gui.argument.TextboxArgument;

/**
 *
 * @author Oliver
 */
@Checkbox(argumentName = "isEnabled1", title = "Title", description = "description", def = true, uiGroup = "Group 1")
@Checkbox(argumentName = "isEnabled2", title = "Title", description = "description", def = true)
@Checkbox(argumentName = "isEnabled3", title = "Title", description = "description", def = true)
@Checkbox(argumentName = "isEnabled4", title = "Title", description = "description", def = false)
@Checkbox(argumentName = "isEnabled5", title = "Title", description = "description", def = true)
@Textbox(argumentName = "inputText1", title = "Today", description = "Todays date", def = "Is it the 23rd?", uiGroup = "Group 1")
@Textbox(argumentName = "inputText2", title = "Yesterday", description = "What day was it tomorrow?")
@Textbox(argumentName = "inputText3", title = "Christmas", description = "This should probably be the 25th Dec")
@Textbox(argumentName = "inputText4", title = "Birthday", description = "This is your birthday")
@Textbox(argumentName = "inputText5", title = "Tomorrow", description = "Tomorrows date", uiGroup = "Group 1")
@Numberbox(argumentName = "numberBox1", title = "Today", description = "Todays date", def = 10)
@Numberbox(argumentName = "numberBox2", title = "Yesterday", description = "What day was it tomorrow?", def = 33)
@Numberbox(argumentName = "numberBox3", title = "Christmas", description = "This should probably be the 25th Dec", def = 1, uiGroup = "Group 1")
@Numberbox(argumentName = "numberBox4", title = "Birthday", description = "This is your birthday", def = 2555, uiGroup = "Group 1")
@Numberbox(argumentName = "numberBox5", title = "Tomorrow", description = "Tomorrows date", def = 522, uiGroup = "Group 1")
@Dropdown(argumentName = "dropDown1", title = "Today", description = "Description for now", def = 2, options = { "Item 1", "Item 2", "Item 3", "Item 4" })
@Dropdown(argumentName = "dropDown2", title = "Yesterday", options = { "Second", "Third" }, uiGroup = "Group 1")
@Path(argumentName = "pathBox1", title = "Today", description = "Todays date", def = "C:/path/to/folder/")
@Path(argumentName = "pathBox2", title = "Yesterday", description = "What day was it tomorrow?")
@Path(argumentName = "pathBox3", title = "Christmas", description = "This should probably be the 25th Dec")
@Path(argumentName = "pathBox4", title = "Birthday", description = "This is your birthday")
@Path(argumentName = "pathBox5", title = "Tomorrow", description = "Tomorrows date")
@Slider(argumentName = "slider1", title = "Slider number 1", min = 1, max = 20, def = 5)
@Slider(argumentName = "slider2", title = "Yesterday", description = "What day was it tomorrow?", def = 29)
@Slider(argumentName = "slider3", title = "Christmas", description = "This should probably be the 25th Dec", min = 1, max = 1, def = 1)
@Slider(argumentName = "slider4", title = "Birthday", description = "This is your birthday", min = 3, max = 445, def = 22)
@Slider(argumentName = "slider5", title = "Tomorrow", description = "Tomorrows date", max = 10, def = 1)
public class Jgui{

	private final HashMap<String, List<Argument>> arguments;

	public Jgui(Class clazz){
		Checkbox[] checkboxes = (Checkbox[]) clazz.getAnnotationsByType(Checkbox.class);
		Dropdown[] dropdowns = (Dropdown[]) clazz.getAnnotationsByType(Dropdown.class);
		Numberbox[] numberboxes = (Numberbox[]) clazz.getAnnotationsByType(Numberbox.class);
		Path[] paths = (Path[]) clazz.getAnnotationsByType(Path.class);
		Slider[] sliders = (Slider[]) clazz.getAnnotationsByType(Slider.class);
		Textbox[] textboxes = (Textbox[]) clazz.getAnnotationsByType(Textbox.class);

		// Convert the annotations to arguments
		arguments = new HashMap<>();

		for(Checkbox annotation : checkboxes){
			addArgumentToMap(arguments, new CheckboxArgument(annotation.argumentName(), annotation.title(), annotation.description(), annotation.def()), annotation.uiGroup());
		}
		for(Dropdown annotation : dropdowns){
			addArgumentToMap(arguments, new DropdownArgument(annotation.argumentName(), annotation.title(), annotation.description(), annotation.def(), Arrays.asList(annotation.options())), annotation.uiGroup());
		}
		for(Numberbox annotation : numberboxes){
			addArgumentToMap(arguments, new NumberboxArgument(annotation.argumentName(), annotation.title(), annotation.description(), annotation.def()), annotation.uiGroup());
		}
		for(Path annotation : paths){
			addArgumentToMap(arguments, new PathArgument(annotation.argumentName(), annotation.title(), annotation.description(), annotation.def()), annotation.uiGroup());
		}
		for(Slider annotation : sliders){
			addArgumentToMap(arguments, new SliderArgument(annotation.argumentName(), annotation.title(), annotation.description(), new Range(annotation.min(), annotation.max(), annotation.def())), annotation.uiGroup());
		}
		for(Textbox annotation : textboxes){
			addArgumentToMap(arguments, new TextboxArgument(annotation.argumentName(), annotation.title(), annotation.description(), annotation.def()), annotation.uiGroup());
		}
	}

	public void startGui(String commandToRun){
		SwingUtilities.invokeLater(() -> {
			GUI gui = new GUI();

			for(String group : arguments.keySet()){
				gui.startGroup(group);
				arguments.get(group).forEach(gui::addArgument);
				gui.endGroup();
			}

			gui.begin(commandToRun);
		});
	}

	private HashMap<String, List<Argument>> addArgumentToMap(HashMap<String, List<Argument>> arguments, Argument arg, String uiGroup){
		List<Argument> args = arguments.get(uiGroup);
		if(args == null){
			args = new ArrayList<>();
		}
		args.add(arg);
		arguments.put(uiGroup, args);
		return arguments;
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		Jgui gui = new Jgui(Jgui.class);
		gui.startGui("Test");
	}

}
