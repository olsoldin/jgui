/*
 * The MIT License
 *
 * Copyright 2022 Oliver.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jgui.annotation;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import jgui.annotation.grouped.Sliders;

/**
 *
 * @author Oliver
 */
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(value = Sliders.class)
public @interface Slider{

	/**
	 * The name of the argument
	 */
	public String argumentName();

	/**
	 * The title to show on the GUI
	 */
	public String title();

	/**
	 * A short description of what the argument is
	 */
	public String description() default "";

	/**
	 * The default value of the slider
	 */
	public int def();

	/**
	 * The min value of the slider
	 */
	public int min() default 0;

	/**
	 * The max value of the slider
	 */
	public int max() default 100;

	/**
	 * The UI group this item is in
	 */
	public String uiGroup() default "";

}
