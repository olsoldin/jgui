/*
 * The MIT License
 *
 * Copyright 2022 Oliver.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jgui.annotation;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import jgui.annotation.grouped.Dropdowns;

/**
 *
 * @author Oliver
 */
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(value = Dropdowns.class)
public @interface Dropdown{

	/**
	 * The name of the argument
	 */
	public String argumentName();

	/**
	 * The title to show on the GUI
	 */
	public String title();

	/**
	 * A short description of what the argument is
	 */
	public String description() default "";

	/**
	 * The index of the default option in the options list
	 */
	public int def() default 0;

	/**
	 * The list of options to give the user
	 */
	public String[] options();

	/**
	 * The UI group this item is in
	 */
	public String uiGroup() default "";

}
